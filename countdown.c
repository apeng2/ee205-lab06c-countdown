///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down from the current time and date till the last day of the semester (May 13 2022)
//
// Example:
//   ./countdown
//   Reference time: Fri May 13 00:00:00 2022
//   Years: 0  Days: 81  Hours: 22  Minutes: 44  Seconds: 24
//   Years: 0  Days: 81  Hours: 22  Minutes: 44  Seconds: 23
//   Years: 0  Days: 81  Hours: 22  Minutes: 44  Seconds: 22
//   Years: 0  Days: 81  Hours: 22  Minutes: 44  Seconds: 21
//   Years: 0  Days: 81  Hours: 22  Minutes: 44  Seconds: 20
//   ... (until the countdown hits 0).
//
// @author Adrian Peng <apeng2@hawaii.edu>
// @date   19_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <math.h>

int main() {

   struct tm t;

// Get the current time in seconds since January 1, 1970
   time_t current_time = time(NULL);   

// The values for our specific reference time in HST
// The reference time is set till the semester is over
   int reference_year     = 2022;
   int reference_month    = 1;
   int reference_day      = 20;
   int reference_hour     = 1;
   int reference_minute   = 21;
   int reference_second   = 0;
   int reference_dst      = -1;

// Prepare our reference time values to be used in mktime(); 
// This is an extra unnecessary step but it helps me envision the process a bit better.
   t.tm_year = reference_year - 1900;
   t.tm_mon = reference_month;
   t.tm_mday = reference_day;
   t.tm_hour = reference_hour;
   t.tm_min = reference_minute;
   t.tm_sec = reference_second;
   t.tm_isdst = reference_dst;

// Convert all our parameters into time since 1900 in seconds.
   time_t reference_time  = mktime(&t);

// Print our reference time into a human readable format
   struct tm *convert_step_1 = localtime(&reference_time);
   char *readable_time = asctime(convert_step_1);
   printf("Reference Time: %s", readable_time );

// Find the time difference between the reference time and current time
   time_t time_difference = reference_time - current_time;

   do {

   int num_years = floor(time_difference / 31536000);
   int num_years_to_sec = num_years * 31536000;
   int sub_step_1 = time_difference - num_years_to_sec;

   int num_days = floor(sub_step_1 / 86400);
   int num_days_to_sec = num_days * 86400;
   int sub_step_2 = time_difference - (num_years_to_sec + num_days_to_sec);

   int num_hours = floor(sub_step_2 / 3600);
   int num_hours_to_sec = num_hours * 3600;
   int sub_step_3 = time_difference - (num_years_to_sec + num_days_to_sec + num_hours_to_sec);

   int num_mins = floor(sub_step_3 / 60);
   int num_mins_to_sec = num_mins * 60;
   int sub_step_4 = time_difference - (num_years_to_sec + num_days_to_sec + num_hours_to_sec + num_mins_to_sec);

   int num_sec = sub_step_4;

   printf("Years: %d  Days: %d  Hours: %d  Minutes: %d  Seconds: %d\n", num_years, num_days, num_hours, num_mins, num_sec);  

   time_difference = time_difference - 1;

   sleep(1);

   } while( time_difference != 0);
   
   printf("Time is up!\n");   
   
   return 0;
}
